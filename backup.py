#!/usr/bin/env python3

import argparse
import json
import csv
import subprocess
import datetime
import tempfile

parser = argparse.ArgumentParser("Backup script")
parser.add_argument("--user", help = "User name")
parser.add_argument("--path", help = "Path to backup list CSV")
parser.add_argument("config", help = "Config file")

class Config():
    def __init__(self, args):
        with open(args.config, "r") as f:
            config = json.load(f)
            self.user = config["user"]
            self.path = config["path"]
            self.UUID = config["UUID"]
        if args.user is not None:
            self.user = args.user
        if args.path is not None:
            self.path = args.path

class Btrfs():
    def __init__(self, UUID, mnt):
        self.UUID = UUID
        self.mnt = mnt
        print("Following mount command needs a superuser password")
        mount = subprocess.run(["sudo", "mount", "UUID=" + self.UUID, self.mnt], check = True)
    def __enter__(self):
        return(self)
    def subvolume(self, user, target):
        self.user = user
        self.target = target
    def create(self):
        newSubvol = subprocess.run(["btrfs", "subvolume", "create",
                                    "/".join([self.mnt, self.user, self.target])],
                                   check = True)
    def snapshot(self, source):
        newSubvol = subprocess.run(["btrfs", "subvolume", "snapshot",
                                    "/".join([self.mnt, self.user, source]),
                                    "/".join([self.mnt, self.user, self.target])],
                                   check = True)
    def copy(self, src, dst):
        copy = subprocess.run(["rsync", "-avu", "--mkpath",
                               src, "/".join([self.mnt, self.user, self.target, dst])], check = True)
    def getLast(self):
        subvolPrc = subprocess.run(["ls", "/".join([self.mnt, self.user])], capture_output = True, check = True)
        subvolList = subvolPrc.stdout.decode("utf8").split("\n")
        subvolList.sort()
        return(subvolList[-1])
    def __exit__(self, exc_type, exc_value, traceback):
        umount = subprocess.run(["sudo", "umount", self.mnt], check = True)

config = Config(parser.parse_args())

with tempfile.TemporaryDirectory() as mnt:
    with Btrfs(config.UUID, mnt) as btrfs:
        date = datetime.datetime.now(datetime.timezone.utc).isoformat()
        btrfs.subvolume(config.user, date)
        if btrfs.getLast() == "":
            btrfs.create()
        else:
            btrfs.snapshot(btrfs.getLast())
        with open(config.path) as f:
            freader = csv.reader(f, delimiter = " ")
            for row in freader:
                try:
                    btrfs.copy(row[0], row[1])
                except IndexError:
                    pass

