# Backup script

A python script for incremental backup using `btrfs` snapshot capability and `rsync`.

# Requirements

* `btrfs-progs`
* `rsync`

# Init

Format your drive with `mkfs.btrfs`. Create a btrfs subvolume in root for every user and set it's uid:gid.
Then chmod the script with `$ chmod +x backup.py`

# Usage

## config.json

A simple flat json dictionary object with following keys:

* **user** A user level subvolume on the disk root.
* **path** Path to the CSV (space/tab separated) file containing the backupped files and dirs
* **UUID** UUID of the disk

## Running

`./backup.py config.json`

